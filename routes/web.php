<?php

Route::namespace ('Front')->group(function () {
	Route::get('/', 'HomeController@index');
	Route::get('posts/{slug}', 'HomeController@getPost');
	Route::get('pages/{slug}', 'HomeController@getPage');
	Route::get('categories/{slug}', 'HomeController@getCategory');
});

Route::group(['prefix' => 'admin'], function () {
	Route::get('gitpull', 'HomeController@gitpull');
	Voyager::routes();
});
