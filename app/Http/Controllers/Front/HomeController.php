<?php

namespace App\Http\Controllers\Front;

use App\Category;
use App\Http\Controllers\Controller;
use App\Page;
use App\Post;
use App\Support\Collection;

class HomeController extends Controller {

	public function index() {
		$posts = Post::orderBy('id', 'DESC')->with('category')->paginate(12);
		return view('kiss.index', compact('posts'));
	}

	public function getPost($slug) {
		$post = Post::where('slug', $slug)->with('category', 'author')->firstOrFail();
		return view('kiss.single', compact('post'));
	}

	public function getPage($slug) {
		$page = Page::where('slug', $slug)->firstOrFail();
		return view('kiss.page', compact('page'));
	}

	public function getCategory($slug) {
		$category = Category::where('slug', $slug)->with('posts')->firstOrFail();
		$posts = (new Collection($category->posts))->paginate(12);

		$title = $category->name . ' Archive';
		$subtitle = '[' . $posts->total() . ' items]';
		return view('kiss.index', compact('posts', 'title', 'subtitle'));
	}

	public function gitpull() {
		shell_exec('cd ' . base_path());
		shell_exec('git pull');
		return redirect('admin')->withMessage('git pull successful');
	}
}
