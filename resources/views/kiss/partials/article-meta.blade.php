<div class="tags has-addons level-item">
    <span class="tag is-rounded is-info">
    	<a class="has-text-light" href="https://devrant.com/users/{{$post->devrant_username}}">{{ '@'.$post->devrant_username }}</a>
    </span>
    <span class="tag is-rounded">
    	<a href="https://devrant.com/rants/{{$post->devrant_rant_id}}">{{ $post->created_at->diffForHumans() }}</a>
    </span>
    <span class="tag is-rounded is-primary">
    	<a class="has-text-light" href="{{ url('categories/'.$post->category->slug) }}">{{ $post->category->name }}</a>
    </span>
</div>