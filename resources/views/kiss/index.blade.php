@extends('kiss.layouts.app')

@section('content')
    @if(isset($title))
        <section class="section">
            <div class="column is-8 is-offset-2 has-text-centered">
                <h2 class="title">{{$title}}</h2>
                @if(isset($subtitle))<p class="subtitle ">{{ $subtitle }}</p>@endif
            </div>
        </section>
    @endif

    @forelse($posts as $post)
        <div class="column is-8 is-offset-2">
            <div class="box">
                <article class="media">
                    <div class="media-content has-text-centered">
                        <p class="title"><a class="has-text-dark" href="{{url('posts/'.$post->slug)}}">{{ $post->title }}</a></p>
                        @include('kiss.partials.article-meta', $post)
                    </div>
                </article>
            </div>
        </div>
    @empty
    @endforelse

    @if($posts->hasPages())
        <section class="section">
            <div class="column is-8 is-offset-2">
                {{ $posts->links('vendor.pagination.bulma') }}
            </div>
        </section>
    @endif
@endsection
