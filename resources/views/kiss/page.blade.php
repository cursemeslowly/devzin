@extends('kiss.layouts.app')

@section('content')
    <div class="column is-8 is-offset-2">
        <div class="card">
            <div class="card-content">
                <p class="title has-text-centered">{{ $page->title }}</p>
                <div class="content">
                    {!! $page->body !!}
                </div>
            </div>
        </div>
    </div>
@endsection