@extends('kiss.layouts.app')

@section('content')
    <div class="column is-8 is-offset-2">
        <div class="box">
            @if($post->excerpt != null)
                <div class="tile notification">
                    <div class="content">
                        {{ $post->excerpt }}
                        <p class="is-size-7 is-italic has-text-right">{{ $post->author->name }}</p>
                    </div>
                </div>
            @endif
            <article class="media">
                <div class="media-content">
                    <p class="title has-text-centered">{{ $post->title }}</p>
                    @include('kiss.partials.article-meta', $post)
                    <div class="content">
                        {!! $post->body !!}
                    </div>
                </div>
            </article>
        </div>
    </div>
@endsection