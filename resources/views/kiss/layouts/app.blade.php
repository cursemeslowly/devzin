<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ setting('site.title', 'Devzin') }}</title>
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('favicon.ico')}}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.1/css/bulma.min.css" />
    <link rel="stylesheet" href="{{asset('kiss/css/style.css')}}">
    @yield('css')
</head>

<body>
    <nav class="navbar" role="navigation" aria-label="main navigation">
        <div class="container">
            <div class="navbar-brand">
                <a class="navbar-item" href="{{url('/')}}">
                    <img src="{{Voyager::image(setting('site.logo'))}}" alt="{{ setting('site.title', 'Devzin') }}" title="{{ setting('site.title', 'Devzin') }}">
                </a>
                <a role="button" class="navbar-burger" data-target="navMenu" aria-label="menu" aria-expanded="false">
                    <span aria-hidden="true"></span>
                    <span aria-hidden="true"></span>
                    <span aria-hidden="true"></span>
                </a>
            </div>
            <div id="navMenu" class="navbar-menu">
                <div class="navbar-end">
                    <a class="navbar-item" href="{{url('/')}}">
                        Home
                    </a>
                    <a class="navbar-item" href="{{url('pages/about')}}">
                        About
                    </a>
                </div>
            </div>
        </div>
    </nav>

    <section class="hero is-bold is-primary">
        <div class="hero-body">
            <div class="container has-text-centered">
                <h1 class="title">
                    Curated magazine for devRant.
                </h1>
                <h2>
                    Poorly executed product for @Alice, @CozyPlanes and many other devRant users.<br>Solution towards the fragile and volatile nature of devRant's rants.
                </h2>
            </div>
        </div>
    </section>

    <div class="container">
        @yield('content')
    </div>

    <footer class="footer">
        <div class="content has-text-centered">
            <p>
                <strong>{{ setting('site.title', 'Devzin') }}</strong> by <a href="{{setting('site.gitlab_user', 'https://gitlab.com/cursemeslowly')}}">cursee</a>. The source code is do-whatever-you-want licensed @ <a href="{{setting('site.gitlab_project','https://gitlab.com/cursemeslowly')}}"><span class="icon"><i class="fab fa-gitlab"></i></span></a>
            </p>
        </div>
    </footer>

    <script src="{{asset('kiss/js/app.js')}}"></script>
</body>
</html>
