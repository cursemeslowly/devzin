<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>{{ config('app.name', 'Devzin') }}</title>
        <meta name="description" content="Template by Colorlib" />
        <meta name="keywords" content="HTML, CSS, JavaScript, PHP" />
        <meta name="author" content="DryThemes" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link rel="shortcut icon" href="images/favicon.png" />
        <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700%7CPT+Serif:400,700' rel='stylesheet' type='text/css'>
        <link type="text/css" rel="stylesheet" href="{{ asset('droppler/css/clear.css') }}"/>
        <link type="text/css" rel="stylesheet" href="{{ asset('droppler/css/common.css') }}"/>
        <link type="text/css" rel="stylesheet" href="{{ asset('droppler/css/font-awesome.min.css') }}"/>
        <link type="text/css" rel="stylesheet" href="{{ asset('droppler/css/carouFredSel.css') }}"/>
        <link type="text/css" rel="stylesheet" href="{{ asset('droppler/css/prettyPhoto.css') }}"/>
        <link type="text/css" rel="stylesheet" href="{{ asset('droppler/css/sm-clean.css') }}"/>
        <link type="text/css" rel="stylesheet" href="{{ asset('droppler/css/style.css') }}"/>

        <!--[if lt IE 9]>
            <script src="{{ asset('droppler/js/html5shiv.js') }}"></script>
            <script src="{{ asset('droppler/js/respond.min.js') }}"></script>
        <![endif]-->
        @yield('css')
    </head>
    <body class="post single-post">

        <table class="doc-loader">
            <tbody>
                <tr>
                    <td>
                        <img src="{{asset('droppler/images/ajax-document-loader.gif')}}" alt="Loading...">
                    </td>
                </tr>
            </tbody>
        </table>


        <nav id="header-main-menu">
            <ul class="main-menu sm sm-clean">
                <li>
                    <a href="{{ url('/') }}">Home</a>
                </li>
                <li>
                    <a href="{{ url('about') }}">About</a>
                </li>
            </ul>
            {{-- <form role="search" class="search-form">
                <label>
                    <input type="search" class="search-field" placeholder="Search …" value="" name="s" title="Search for:">
                </label>
            </form> --}}
        </nav>



        @yield('content')



        <footer class="footer">
            <div class="content-1170 center-relative">
                <ul>
                    <li class="left-footer-content">
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>. Brought to you by <a href="https://devrant.com/users/cursee">Cursee</a> <i class="fa fa-hand-peace-o" aria-hidden="true"></i>
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    </li>
                </ul>
                <div class="clear"></div>
            </div>
        </footer>

        <div class="fixed scroll-top">
            <img src="{{asset('droppler/images/back_to_top.png')}}" alt="Go Top">
        </div>

        <!--Load JavaScript-->
        <script src="{{asset('droppler/js/jquery.js')}}"></script>
        <script src="{{asset('droppler/js/particles.min.js')}}"></script>
        <script src="{{asset('droppler/js/app.js')}}"></script>
        <script src="{{asset('droppler/js/jquery.fitvids.js')}}"></script>
        <script src="{{asset('droppler/js/jquery.smartmenus.min.js')}}"></script>
        <script src="{{asset('droppler/js/isotope.pkgd.js')}}"></script>
        <script src="{{asset('droppler/js/imagesloaded.pkgd.js')}}"></script>
        <script src="{{asset('droppler/js/isotope.pkgd.js')}}"></script>
        <script src="{{asset('droppler/js/jquery.carouFredSel-6.0.0-packed.js')}}"></script>
        <script src="{{asset('droppler/js/jquery.mousewheel.min.js')}}"></script>
        <script src="{{asset('droppler/js/jquery.touchSwipe.min.js')}}"></script>
        <script src="{{asset('droppler/js/jquery.easing.1.3.js')}}"></script>
        <script src="{{asset('droppler/js/imagesloaded.pkgd.js')}}"></script>
        <script src="{{asset('droppler/js/jquery.prettyPhoto.js')}}"></script>
        <script src="{{asset('droppler/js/jquery.nicescroll.min.js')}}"></script>
        <script src="{{asset('droppler/js/main.js')}}"></script>
        @yield('js')
    </body>
</html>
