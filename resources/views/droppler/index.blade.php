@extends('droppler.layouts.app')

@section('content')
    @forelse($posts as $post)
    <article class="entry-holder blog-item-holder no-background-image" style="background-image:url({{ Voyager::image( $post->image ) }});">
        <div class="entry-content relative">
            <div class="content-1170 center-relative center-text">
                <div class="entry-date published">{{ $post->created_at->diffForHumans() }}</div>
                @if($post->category!=null)
                <div class="cat-links">
                    <ul>
                        <li>
                            <a href="#">{{ $post->category->name }}</a>
                        </li>
                    </ul>
                </div>
                @endif
                <h2 class="entry-title">
                    <a href="{{url('posts/'.$post->slug)}}">
                        {{ $post->title }}
                    </a>
                </h2>
                <a href="{{url('posts/'.$post->slug)}}">
                    <div class="read-more-holder">
                        <img src="{{asset('/droppler/images/read_more_icon.svg')}}" alt="Read more.">
                    </div>
                    <div class="read-more-text">READ MORE</div>
                </a>
                <div class="clear"></div>
            </div>
        </div>
    </article>
    @empty
    @endforelse
@endsection